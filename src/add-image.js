import flower from "./catimg.jpg";

export default function addImage() {
  const img = document.createElement("img");
  img.alt = "flower";
  img.width = 300;
  img.src = flower;
  document.body.appendChild(img);
}
